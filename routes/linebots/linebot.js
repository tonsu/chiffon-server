import express from 'express';
import dotenv from 'dotenv';
import https from 'https';

const router = express();
dotenv.config();

const config = {
	channelAccessToken: process.env.CHANNEL_ACCESS_TOKEN,
	channelSecret: process.env.CHANNEL_SECRET,
};


router.post("/manydev", async function(req, res, next) {
	res.send("HTTP POST request sent to the webhook URL!")
	// If the user sends a message to your bot, send a reply message


	if (req.body.events[0].type === "message") {

		const messages =  [
			{
				"type": "text",
				"text": "Hello, user"
			},
			{
				"type": "text",
				"text": "May I help you?"
			}
		]

		if(req.body.events[0].source && req.body.events[0].source.userId) {
			messages.push({
				"type": "text",
				"text": `your user_id: ${req.body.events[0].source.userId}`
			})
		}
		// Message data, must be stringified
		const dataString = JSON.stringify({
			replyToken: req.body.events[0].replyToken,
			messages: messages
		})

		// Request header
		const headers = {
			"Content-Type": "application/json",
			"Authorization": "Bearer " + config.channelAccessToken
		}

		// Options to pass into the request
		const webhookOptions = {
			"hostname": "api.line.me",
			"path": "/v2/bot/message/reply",
			"method": "POST",
			"headers": headers,
			"body": dataString
		}

		// Define request
		const request = https.request(webhookOptions, (res) => {
			res.on("data", (d) => {
				process.stdout.write(d)
			})
		})

		// Handle error
		request.on("error", (err) => {
			console.error(err)
		})

		// Send data
		request.write(dataString)
		request.end()
	}
});

router.get('/push', async (req, res, next) => {
	res.send('Welcome to Line Bots');
});

router.get('/', async (req, res, next) => {
	res.send('Welcome to Line Bots');
});

export default router;
