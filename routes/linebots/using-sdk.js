import line from '@line/bot-sdk';
import express from 'express';
import ApiError from '../../error/ApiError.js';
import dotenv from 'dotenv';

const router = express();
dotenv.config();


const config = {
	channelAccessToken: process.env.CHANNEL_ACCESS_TOKEN,
	channelSecret: process.env.CHANNEL_SECRET,
};


const client = new line.Client(config);

function handleEvent (event) {
	if (event.type !== 'message' || event.message.type !== 'text') {
		// ignore non-text-message event
		return Promise.resolve(null);
	}

	let text = event.message.text;
	if (event.message.text === 'Hello') text = 'Hello!?!';
	else if (event.message.text === 'Hi') text = 'Hi!?!';

	// create a echoing text message
	const echo = { type: 'text', text };

	// use reply API
	return client.replyMessage(event.replyToken, echo);
}

router.post('/manydev', line.middleware(config), async (req, res, next) => {
	try {
		Promise
			.all(req.body.events.map(handleEvent))
			.then((result) => res.json(result))
			.catch((err) => {
				console.error(err);
				res.status(500).end();
			});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.get('/', async (req, res, next) => {
	res.send('Welcome Nancy Line Bots');
});

export default router;
