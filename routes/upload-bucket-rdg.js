import ApiError from '../error/ApiError.js';
import resizeFromFileOrUrl2 from '../media/resizeFromFileOrUrl2_RDG.js';
import multer from 'multer';
import dayjs from "dayjs";
import express from 'express';
import dotenv from 'dotenv';
import { uploadFileToBucket } from "../media/s3_RDG.js";
import fs from "fs-extra";

dotenv.config();

const TEMP_PATH = 'public/temp';

const router = express();
const storage = multer.memoryStorage();
const uploads = multer({ storage });

const diskStorage = multer.diskStorage({
	destination: (req, file, cb) => {
		fs.mkdirsSync(TEMP_PATH);
		cb(null, TEMP_PATH);
	},
	filename: (req, file, cb) => {
		console.log('file.originalname', file.originalname);
		cb(null, file.originalname)
	},
	// filename: (req, file, cb) => {
	// 	// randomBytes function will generate a random name
	// 	let customFileName = crypto.randomBytes(18).toString('hex');
	// 	// get file extension from original file name
	// 	let fileExtension = path.extname(file.originalname).split('.')[1];
	// 	cb(null, customFileName + '.' + fileExtension);
	// },
});

const discUploads = multer({ storage: diskStorage });

const UPLOAD_TO_S3 = true;

const generateUniqueName = () => {
	let name = dayjs().format('YYMMDDHHmmss_SSS' + Math.floor(1000 + Math.random() * 9000));
};

router.post('/image', uploads.single('file'), async (req, res, next) => {
	try {

		let settings = req.body.settings;

		if (typeof settings === 'string') settings = JSON.parse(settings);
		let params = { settings };

		if (req.body.url) {
			params.src = req.body.url;
		} else if (req.file) {
			params.file = req.file;
		} else {
			throw 'missing url or file';
		}

		const result = await resizeFromFileOrUrl2(params);

		res.status(200).send({
			result,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/file', discUploads.single('file'), async (req, res, next) => {
	try {

		let settings = req.body.settings;
		if (typeof settings === 'string') settings = JSON.parse(settings);
		const fullPathTemp = `${TEMP_PATH}/${settings.name}`
		const fullPath = `${settings.path}/${settings.name}`

		const resUpload = await uploadFileToBucket(fullPathTemp, fullPath, settings.bucketName);
		fs.unlinkSync(fullPathTemp);

		res.status(200).send({
			result: 'success',
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
