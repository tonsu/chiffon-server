import express from 'express';
import ApiError from '../error/ApiError.js';
import dbConnect from '../configs/dbConnect.js';
import User from '../models/User.js';
const router = express();
import dotenv from 'dotenv';
dotenv.config();

function getSchema (key){
	switch (key){
		case 'users':
			return User
		case 'models':
			return Model
		default:
			return null
	}

	if(key==='users') {
		return User
	}

}

router.get('/dbquery', async (req, res, next) => {

	try {
		const { collection, query } = req.params;
		await dbConnect();
		const _query = query || {};
		let data =  await getSchema(collection).find(_query);
		res.status(200).send(data);
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/dbquery', async (req, res, next) => {

	try {
		const { collection, query } = req.body;
		await dbConnect();
		let data = null;
		getSchema(collection)
		if (collection == 'users') {
			const _query = query || {};
			data = await User.find(_query);
		}
		res.status(200).send(data);
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
