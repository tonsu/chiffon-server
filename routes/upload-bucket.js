import ApiError from '../error/ApiError.js';
import resizeFromFileOrUrl2 from '../media/resizeFromFileOrUrl2.js';
import multer from 'multer';
import dayjs from "dayjs";
import express from 'express';
import dotenv from 'dotenv';

const router = express();
const storage = multer.memoryStorage();
const uploads = multer({ storage });
dotenv.config();

const UPLOAD_TO_S3 = true;

const generateUniqueName = () => {
	let name = dayjs().format('YYMMDDHHmmss_SSS' + Math.floor(1000 + Math.random() * 9000));
};

router.post('/image', uploads.single('file'), async (req, res, next) => {
	try {
		let settings = req.body.settings;

		if (typeof settings === 'string') settings = JSON.parse(settings);
		let params = { settings };

		if (req.body.url) {
			params.src = req.body.url;
		} else if (req.file) {
			params.file = req.file;
		} else {
			throw 'missing url or file';
		}

		const result = await resizeFromFileOrUrl2(params);
		res.status(200).send({ result });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});
export default router;
