import multer from 'multer';
import { getFileStream } from '../media/s3.js';
import express from 'express';
import ApiError from '../error/ApiError.js';
import dotenv from 'dotenv';
import vCard from 'vcards-js';

const router = express();
dotenv.config();
const storage = multer.memoryStorage();

/**
 * Is this about forwarding content?
 */
router.get('/', async (req, res, next) => {
	try {

		//create a new vCard
		let vc = vCard();

		//set properties
		vc.firstName = 'Eric';
		vc.middleName = 'J';
		vc.lastName = 'Nesser';
		vc.organization = 'ACME Corporation';
		vc.photo.attachFromUrl('https://avatars2.githubusercontent.com/u/5659221?v=3&s=460')

			//set content-type and disposition including desired filename
		res.set('Content-Type', 'text/vcard; name="enesser.vcf"');
		res.set('Content-Disposition', 'inline; filename="enesser.vcf"');

		//send the response
		res.send(vc.getFormattedString());
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/', async (req, res, next) => {
	try {
		req.body
		//create a new vCard
		let vc = vCard();
		// vCard.firstName = 'Eric';
		// vCard.middleName = 'J';
		// vCard.lastName = 'Nesser';
		// vCard.organization = 'ACME Corporation';
		// vCard.photo.attachFromUrl('https://avatars2.githubusercontent.com/u/5659221?v=3&s=460', 'JPEG');
		// vCard.workPhone = '312-555-1212';
		// vCard.birthday = new Date('01-01-1985');
		// vCard.title = 'Software Developer';
		// vCard.url = 'https://github.com/enesser';
		// vCard.note = 'Notes on Eric';
		//
		// //set properties
		// vc.firstName = 'Eric';
		// vc.middleName = 'J';
		// vc.lastName = 'Nesser';
		// vc.organization = 'ACME Corporation';

		//set content-type and disposition including desired filename
		res.set('Content-Type', 'text/vcard; name="enesser.vcf"');
		res.set('Content-Disposition', 'inline; filename="enesser.vcf"');

		//send the response
		res.send(vc.getFormattedString());
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});
export default router;
