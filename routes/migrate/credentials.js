import express from 'express';
import dotenv from "dotenv";
import dbConnect from "../../configs/dbConnect.js";
import Credential from "../../models/Credential.js";
import ApiError from "../../error/ApiError.js";

const router = express();
dotenv.config();

const initCredentials = [
	{
		key: 'ap-single-dropship-1987',
		slug: 'ap-single-dropship',
		services: {
			"s3": {
				"id": "AKIAZVTFDTWHEQHK7SGV",
				"secret": "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",
				"region": "ap-southeast-1",
				"bucket_name": "ap-single-dropship",
			},
			"local-storage": { "path": "ap-single-dropship" },
			"mongodb": {
				uri: "mongodb+srv://dev-artplore:arstarst1qwfpqwfp2zxcvzxcv3@cluster0.pnkte.mongodb.net/ap-single-dropship?retryWrites=true&w=majority",
			},
		},
	},
];

// --------------------------------------------------------------------------------
// export
// --------------------------------------------------------------------------------

router.post('/', async (req, res, next) => {
	try {
		await dbConnect();
		await Credential.deleteMany({});
		const credentials = await Credential.create(initCredentials);
		res.status(200).send({
			status: 'success',
			credentials,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
