import express from 'express';
import credentials from "./credentials.js";

const router = express();

router.use('/credentials', credentials);

export default router;