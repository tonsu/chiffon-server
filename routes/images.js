import multer from 'multer';
import { getFileStream } from '../media/s3.js';
import express from 'express';
import ApiError from '../error/ApiError.js';
import dotenv from 'dotenv';

const router = express();
dotenv.config();
const storage = multer.memoryStorage();

/**
 * Is this about forwarding content?
 */
router.get('/:key', async (req, res, next) => {
	try {
		const readStream = getFileStream(req.params.key.replace('$$$', '/'));
		readStream.pipe(res);
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});
export default router;
