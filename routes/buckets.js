import ApiError from '../error/ApiError.js';
import multer from 'multer';
import express from 'express';
import dotenv from 'dotenv';
import S3 from 'aws-sdk/clients/s3.js';
import Credential from '../models/Credential.js';
import dbConnect from "../configs/dbConnect.js";
import User from "../models/User.js";
import { getBucketCredential } from "../inc/dbUtils.js";

const router = express();
const storage = multer.memoryStorage();
const uploads = multer({ storage });
dotenv.config();

const region = process.env.AWS_BUCKET_REGION;
const accessKeyId = process.env.AWS_ID;
const secretAccessKey = process.env.AWS_SECRET;

const s3 = new S3({
	region,
	accessKeyId,
	secretAccessKey,
});

router.get('/list-objects', uploads.single('file'), async (req, res, next) => {
	try {
		const { prefix = '' } = req.query;
		await dbConnect();
		const bucketClient = await getBucketCredential(req)
		const objects = await bucketClient.client.listObjects({
			Bucket: bucketClient.bucketName,
			Prefix: prefix,
		}).promise();

		res.status(200).send({
			objects,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/delete-objects', uploads.single('file'), async (req, res, next) => {
	try {
		let { bucket, keys } = req.body;
		if(!keys) throw 'no keys'
		if(typeof keys === 'string') keys = [keys]
		const objects = keys.map(v=>({Key: v.trim()}))
		const bucketClient = await getBucketCredential(req)
		const deleted_objects = await bucketClient.client.deleteObjects({
			Bucket: bucket,
			Delete: { Objects: objects },
		}).promise();

		res.status(200).send({
			deleted_objects: deleted_objects,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});
export default router;

// ffmpeg -i https://img-9gag-fun.9cache.com/photo/aXreg4D_460svav1.mp4 ~/htdocs/temp/arst.mp4

//2021/10/211003233438_3019956-lg.jpg