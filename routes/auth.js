import express from 'express';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import ApiError from '../error/ApiError.js';
import { commonError } from '../error/error-utils.js';
import dbConnect from '../configs/dbConnect.js';
import User from '../models/User.js';
import verify from '../configs/verifyToken.js';
import dotenv from 'dotenv';

const router = express();
dotenv.config();

const DEFAULT_ROLES = [
	'moderator',
];

/* POST login. */
router.post('/login-passport', (req, res, next) => {
	passport.authenticate('local', { session: false }, (err, user, info) => {
		if (err) return next(err);
		if (user) {
			return res.json(user);

			// const token = jwt.sign(user, process.env.JWT_SECRET);
			// return res.json({ user, token });
		} else {
			return res.status(422).json(info);
		}
	})(req, res, next);
});

router.post('/register', async (req, res, next) => {
	try {
		await dbConnect();
		const user = await User.create(
			{
				username: req.body.username,
				password: req.body.password,
				display_name: req.body.display_name,
				email: req.body.email,
				roles: req.body.roles || DEFAULT_ROLES,
			},
		); /* create a new model in the database */
		res.status(201).json({ success: true, data: user });
	} catch (err) {
		if (err.errors) {
			if (err.errors.email) {
				next(ApiError.badRequest(commonError(err.errors.email.message)));
				return;
			}
			if (err.errors.username) {
				next(ApiError.badRequest(commonError(err.errors.username.message)));
				return;
			}
		}
		next(ApiError.badRequest(commonError(err)));
	}
});

router.post('/login', async (req, res, next) => {

	const username = req.body.username.toLowerCase();
	const password = req.body.password;

	try {
		await dbConnect();
		const user = await User.findOne({ $or: [{ username: username }, { email: username }] });
		if (user) {
			try {
				const promise = new Promise((resolve, reject) => {
					user.comparePassword(password, function (err, isMatch) {
						if (!err && isMatch) {
							resolve({
								email: user.email,
								name: user.display_name,
								image: null,
								roles: user.roles,
							});
						}
						reject();
					});
				});
				const ret = await promise;
				const token = jwt.sign({
					id: user._id,
					display_name: user.display_name,
					email: user.email,
					roles: user.roles,
				}, process.env.JWT_SECRET, {
					expiresIn: "5d",
				});

				// Return data here!
				res.json({
					token,
					roles: user.roles,
					user: {
						display_name: user.display_name,
					}
				});

			} catch (err) {
				throw ('wrong username or password');
			}
		} else {
			throw ('the user is not exist');
		}
	} catch (err) {
		next(ApiError.badRequest(err));
	}

});

router.get('/check', verify, async (req, res, next) => {
	console.log('123', 123);
	res.send({ success: true });
});

router.post('/login-cheer', async (req, res, next) => {
	try {
		if (req.body.password.toString() === '1112') {
			res.json({
				token: jwt.sign({ valid: true }, process.env.JWT_PAGE_SECRET, {
					expiresIn: "5m",
				}),
			});
		} else {
			next(ApiError.badRequest('Incorrect password'));
		}
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/user-data', verify, async (req, res, next) => {
	const user = req.user;
	res.json({ user });
});

export default router;
