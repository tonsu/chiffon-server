import ApiError from '../error/ApiError.js';
import resizeFromFileOrUrl from '../media/resizeFromFileOrUrl.js';
import { uploadFileToBucket } from '../media/s3.js';
import sharp from "sharp";
import multer from 'multer';
import dayjs from "dayjs";
import express from 'express';
import dotenv from 'dotenv';

const router = express();
const storage = multer.memoryStorage();
const uploads = multer({ storage });
dotenv.config();

const UPLOAD_TO_S3 = true;

router.post('/image', uploads.single('file'), async (req, res, next) => {
	try {
		const today = new Date();
		const year = today.getFullYear();
		const month = `${today.getMonth() + 1}`.padStart(2, "0");
		let subPath = req.body.path || `${year}/${month}`;
		let name = req.body.name || dayjs().format('YYMMDDHHmmss_SSS' + Math.floor(1000 + Math.random() * 9000));

		let results = [];

		let params = {
			outputs: [
				{
					fullPath: `./public/uploads/${subPath}/${name}-sm$DUPLICATE$.jpg`,
					overwrite: true,
					name: 'sm',
					resize: {
						width: 150,
						height: 150,
						fit: 'cover',
					},
				},
				{
					fullPath: `./public/uploads/${subPath}/${name}-lg$DUPLICATE$.jpg`,
					overwrite: true,
					name: 'lg',
					resize: {
						width: 1080,
						height: 1080,
						fit: sharp.fit.inside,
						withoutEnlargement: true,
					},
				},
			],
		};

		if (req.body.url) {
			params.src = req.body.url;
		} else if (req.file) {
			// console.log('req.body.file', req.body.file);
			params.file = req.file;
		} else {
			throw 'missing url or file';
		}

		results = await resizeFromFileOrUrl(params);

		if (UPLOAD_TO_S3) {
			for (let key of Object.keys(results)) {
				// https://chiffon-drive.s3.ap-southeast-1.amazonaws.com
				const file = results[key]
				const resUpload = await uploadFileToBucket(file.serverPath, `${subPath}/${file.name}`, req.body.bucket_name);
				// file['key'] = resUpload['key'];
			}
		}

		res.status(200).send({
			images: results,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});
export default router;
