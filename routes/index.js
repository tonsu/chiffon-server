import express from 'express';
import auth from './auth.js';
import users from './api/users.js';
import uploadBucket from './upload-bucket.js';
import uploadBucketRDG from './upload-bucket-rdg.js';
import upload from './upload.js';
import images from './images.js';
import data from './data.js';
import verify from '../configs/verifyToken.js';
import buckets from "./buckets.js";
import lineBots from "./linebots/linebot.js";
import vcard from "./vcard.js";
import credentials from "./api/credentials.js";
import api from "./api/index.js";
import migrate from "./migrate/index.js";

const router = express();

router.get('/', (req, res) => {
	res.send('Welcome Chiffon Server: v. image upload');
});

router.use('/auth', auth);
// router.use('/users', verify, users);
router.use('/users', users);
router.use('/upload', upload);
router.use('/upload-bucket', uploadBucket);
router.use('/upload-bucket-rdg', uploadBucketRDG);
router.use('/images', images);
router.use('/data', data);
router.use('/buckets', buckets);
router.use('/linebots', lineBots);
router.use('/vcard', vcard);
router.use('/credentials', credentials);
router.use('/api', api);
router.use('/migrate', migrate);

export default router;