import express from 'express';
import dbConnect from "../../configs/dbConnect.js";
import Structure from "../../models/Structure.js";
import ApiError from "../../error/ApiError.js";
import User from "../../models/User.js";
import { commonError } from '../../error/error-utils.js';

const router = express();

function capitalize (word) {
	return word
		.toLowerCase()
		.replace(/\w/, firstLetter => firstLetter.toUpperCase());
}

function getDisplayName (username) {
	return username.replace(/[-_]/g, ' ').split(' ').map(v => capitalize(v)).join(' ');
}

router.get('/', async (req, res, next) => {
	try {
		let { limit, page } = req.query;
		await dbConnect();
		limit = parseInt(limit);
		page = parseInt(page);
		if (!limit && !page) {
			limit = 99;
			page = 1;
		}
		let result = await User.paginate({ deleted_at: null }, { page, limit });
		res.status(200).send({
			users: result.docs,
			pagination: {
				limit: result.limit,
				page: result.page,
				pages: result.pages,
				total: result.total,
			},
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

/* GET user profile. */
router.get('/profile', (req, res, next) => {
	res.send(req.user);
});

router.get('/:id/view', async (req, res, next) => {
	try {
		await dbConnect();
		let user = {}; // may put default value here
		if (req.params.id !== 'none') {
			user = await User.findOne({ _id: req.params.id });
		}
		let structure = await Structure.findOne({ struct_id: 'user' });
		res.status(200).send({ user, structure });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/', async (req, res, next) => {
	try {
		await dbConnect();
		const params = Object.assign({}, req.body);
		if (!params.display_name && params.username) {
			params.display_name = getDisplayName(req.body.username);
		}
		const result = await User.create(params);
		res.status(201).json({ user: result });
	} catch (err) {
		next(ApiError.badRequest(commonError(err)));
	}
});

router.put('/:id', async (req, res, next) => {
	try {
		await dbConnect();
		const params = Object.assign({}, req.body);
		if (!params.display_name && params.username) {
			params.display_name = getDisplayName(req.body.username);
		}
		const result = await User.updateOne({}, params);
		res.status(201).json({ user: result });
	} catch (err) {
		next(ApiError.badRequest(commonError(err)));
	}
});

router.get('/:id/edit', async (req, res, next) => {
	try {
		await dbConnect();
		let user = {}; // may put default value here
		if (req.params.id !== 'none') {
			user = await User.findOne({ _id: req.params.id });
		}
		let structure = await Structure.findOne({ struct_id: 'user' });
		res.status(200).send({ user, structure });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.delete('/:id', async (req, res, next) => {
	try {
		await dbConnect();
		const result = await User.deleteOne({ _id: req.params.id });
		res.status(200).json(result);
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.put('/my-update', async (req, res, next) => {
	try {
		await dbConnect();
		const result = await User.updateMany({ role: { $eq: null } }, req.body);
		res.status(201).json({ success: result });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
