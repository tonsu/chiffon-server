import express from 'express';
import ApiError from "../../../error/ApiError.js";
import dotenv from "dotenv";
import { MongoMine } from "../../../inc/MongoMine.js";
import JSZip from "node-zip";
import fs from "fs";
import { uploadFileToUserBucket } from "../../../media/s3.js";
import dayjs from "dayjs";
import { getCredential } from "../../../inc/credentialUtils.js";
import { getS3Client } from "../../../inc/s3Utils.js";
import { getZipEntries, myUnzip, zipEntryWithCallback } from "../../../inc/zipUtils.js";
import { downloadFile } from "../../../inc/fileUtils.js";
import { pushArrayField } from "../../../inc/utils.js";
import { ObjectID } from "mongodb";

const router = express();
dotenv.config();

const BACKUP_BASE_DIR = 'db-exports';
const TEMP_PATH = 'public/temp';

function sanitize (rows) {
	for (const row of rows) {
		row._id = new ObjectID(row._id);

		// todo
		// for e.g. "order_id" foreign key
		// this will affect the performance
		// use e.g. explicitly provided like
		// [ orders.order_id ]

		for (const key of Object.keys(row)) {
			// ObjectID.isValid(row[key]) alone is not enough (e.g. number)
			if (typeof row[key] === 'string' && ObjectID.isValid(row[key]) && row[key].length >= 20) {
				row[key] = new ObjectID(row[key])
			}
		}

	}
}

// --------------------------------------------------------------------------------
// export
// --------------------------------------------------------------------------------

router.post('/export', async (req, res, next) => {
	try {
		let result = { files: [] };
		const { api_key, db_name, excludes = [], name } = req.body;
		const { slug: userDir, services } = await getCredential(api_key);
		const theDB = new MongoMine(services.mongodb.uri, db_name);

		try {
			await theDB.connect();
			const collections = await theDB.listCollections();
			const zip = new JSZip();

			// Pushing each file to zip buffer
			// ----------------------------------------
			for (const collection of collections) {
				if (excludes.includes(collection.name)) continue;
				const collectionName = collection.name;
				const fileName = collectionName + '.json';
				const rows = await theDB.find({ collection: collectionName });
				zip.file(fileName, JSON.stringify(rows));
				result.files.push(collection.name);
			}

			// Write to file then upload to S3
			// ----------------------------------------
			if (!fs.existsSync(TEMP_PATH)) {
				fs.mkdirSync(TEMP_PATH, { recursive: true });
			}

			const dstFileName = name || `${db_name}_${dayjs().format('YYMMDDHHmmss')}`;

			const localPath = `${TEMP_PATH}/${db_name}_${dayjs().format('YYMMDDHHmmss')}.zip`;
			fs.writeFileSync(localPath, zip.generate({ base64: false, compression: 'DEFLATE' }), 'binary');
			const uploadPath = `${BACKUP_BASE_DIR}/${userDir}/${dstFileName}.zip`;
			await uploadFileToUserBucket({
				file: localPath,
				uploadPath,
				region: services.s3.region,
				accessKeyId: services.s3.id,
				secretAccessKey: services.s3.secret,
				bucketName: services.s3.bucket_name,
			});
			fs.unlinkSync(localPath);

			res.status(200).send({
				status: 'success',
				uploadPath,
				...result,
			});
		} catch (err) {
			throw err;
		} finally {
			await theDB.close();
		}
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

// --------------------------------------------------------------------------------
// restore (get the latest backup)
// --------------------------------------------------------------------------------

function findInListObjects (list, keyOrEtag) {
	for (const item of list) {
		if (item.Key.indexOf(keyOrEtag) > -1 || item.ETag.indexOf(keyOrEtag) > -1) {
			return item;
		}
	}
	return false;
}

router.post('/restore', async (req, res, next) => {
	try {
		const { api_key, db_name, backup_key, excludes = [] } = req.body;
		const { slug: userDir, services } = await getCredential(api_key);
		const theDB = new MongoMine(services.mongodb.uri, db_name);

		// Find latest backup
		// ----------------------------------------
		const s3 = await getS3Client(services.s3);
		const objects = await s3.client.listObjectsV2({
			Bucket: s3.bucketName,
			Prefix: `${BACKUP_BASE_DIR}/${userDir}`,
		}).promise();
		if (objects.Contents.length === 0) throw new Error('no backup found');

		let selectedBackup;
		if (backup_key) {
			selectedBackup = findInListObjects(objects.Contents, backup_key);
			if (!selectedBackup) throw new Error(`cannot find the backup, key: ${backup_key}`);
		} else {
			selectedBackup = objects.Contents[objects.Contents.length - 1]; // Latest
		}

		const result = {
			dbName: db_name,
			collections: {},
		};

		try {
			const zipFileName = selectedBackup.Key.match(/[^\\/]+$/)[0];
			const localZipPath = `${TEMP_PATH}/${zipFileName}`;
			const fileNameOnly = zipFileName.split('.')[0];
			await downloadFile(
				'https://ap-single-dropship.s3.ap-southeast-1.amazonaws.com/' + selectedBackup.Key,
				localZipPath);

			await theDB.connect();
			const entries = getZipEntries(localZipPath, excludes);
			for (const entry of entries) {
				const collection = entry.name;
				if (excludes.includes(collection)) continue;
				const content = JSON.parse(entry.content);
				sanitize(content);
				const infoDelete = await theDB.deleteMany({ collection });
				const infoCreate = await theDB.create(content, { collection });
				result.collections[collection] = {
					deletedCount: infoDelete.deletedCount,
					insertedCount: infoCreate.insertedCount,
				};
			}

			res.status(200).send({
				status: 'success',
				selectedBackup,
				result,
			});

		} catch (err) {
			throw err;
		} finally {
			await theDB.close();
		}
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
