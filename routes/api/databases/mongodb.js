import express from 'express';
import ApiError from "../../../error/ApiError.js";
import dotenv from "dotenv";
import { MongoMine } from "../../../inc/MongoMine.js";
import { getCredential } from "../../../inc/credentialUtils.js";

const router = express();
dotenv.config();

// --------------------------------------------------------------------------------
// List
// --------------------------------------------------------------------------------

router.get('/collections', async (req, res, next) => {

	try {
		let result = {};
		const { services } = await getCredential(req.body?.api_key);
		const theDB = new MongoMine(services.mongodb.uri, req.body?.db_name);

		try {
			await theDB.connect();
			const collections = await theDB.listCollections();
			result = {
				collections,
			};
		} catch (err) {
			console.error(err);
		} finally {
			await theDB.close();
		}

		res.status(200).send({
			status: 'success',
			...result,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
