import express from 'express';
import mongodb from "./mongodb.js";
import mongodbBackup from "./mongodb-backup.js";

const router = express();

router.use('/mongodb', mongodb);
router.use('/mongodb-backup', mongodbBackup);

export default router;
