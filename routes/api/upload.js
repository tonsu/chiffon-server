import fs from 'fs-extra';
import ApiError from '../../error/ApiError.js';
import multer from 'multer';
import express from 'express';
import dotenv from 'dotenv';
import resizeFromFileOrUrl from "../../media/resizeFromFileOrUrlNew.js";
import { uploadFileToBucket } from "../../inc/s3Utils.js";
import dbConnect from "../../configs/dbConnect.js";
import User from "../../models/User.js";
import S3 from "aws-sdk/clients/s3.js";

const router = express();
const storage = multer.memoryStorage();
const uploads = multer({ storage });
dotenv.config();

const UPLOAD_TO_S3 = true;

router.post('/image', uploads.single('file'), async (req, res, next) => {
	try {

		let settings = req.body.settings;

		if (typeof settings === 'string') settings = JSON.parse(settings);
		// let params = { settings };

		let input = {};
		let result = {};

		if (req.body.url) {
			input.src = req.body.url;
		} else if (req.file) {
			input.file = req.file;
		} else {
			throw new Error('missing url or file');
		}

		await dbConnect();
		// todo - check for secret
		const user = await User.findOne({ user_id: req.body.id });
		if (!user) throw new Error('no user for image upload');
		let bucketClient;
		if (hasBucketPath(settings)) {
			const bucket = user.credentials.s3.bucket;
			bucketClient = {
				client: new S3({
					region: bucket.region,
					accessKeyId: bucket.id,
					secretAccessKey: bucket.secret,
				}),
				bucketName: bucket.bucket_name,
			};
		}

		for (const key of Object.keys(settings)) {
			const setting = settings[key];
			if (!setting.path && setting.bucketPath) {
				setting.path = setting.bucketPath;
			}
			const localPath = await resizeFromFileOrUrl(input, setting);
			result[key] = localPath;
			if (setting.bucketPath && bucketClient) {
				const bucketPath = await uploadFileToBucket(bucketClient, setting);
				result[key] = setting.bucketPath;
			}
		}

		res.status(200).send({
			result,
		});

	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

const hasBucketPath = (settings) => {
	for (const key of Object.keys(settings)) {
		console.log('settings[key]', settings[key]);
		if (settings[key].bucketPath) return true;
	}
	return false;
};
export default router;

const arst = { "type": "aws", "id": "AKIAQXXN332NXQMLO76R", "secret": "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", "region": "ap-southeast-1", "bucket_name": "rabbit-intranet" };

const sss = {
	"s3": { "bucket": { "type": "aws", "id": "AKIAQXXN332NXQMLO76R", "secret": "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", "region": "ap-southeast-1", "bucket_name": "rabbit-intranet" } },
	"local-storage": { "path": "rdg-intranet" },
};