import ApiError from '../../error/ApiError.js';
import multer from 'multer';
import express from 'express';
import dotenv from 'dotenv';
import dbConnect from "../../configs/dbConnect.js";
import { getCredential } from "../../inc/credentialUtils.js";
import { getS3Client } from "../../inc/s3Utils.js";

const router = express();
const storage = multer.memoryStorage();
const uploads = multer({ storage });
dotenv.config();

router.get('/', async (req, res, next) => {
	res.status(200).send({
		success: 'true',
	});
});

router.post('/list-objects', async (req, res, next) => {
	try {
		const { prefix = '' } = req.body;
		await dbConnect();
		const { services } = await getCredential(req.body?.api_key);
		const s3 = await getS3Client(services.s3);
		const objects = await s3.client.listObjectsV2({
			Bucket: s3.bucketName,
			Prefix: prefix,
		}).promise();
		res.status(200).send({ objects });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

router.post('/delete-objects', uploads.single('file'), async (req, res, next) => {
	try {
		let { keys } = req.body;
		if (!keys) throw 'no keys';
		if (typeof keys === 'string') keys = [keys];
		const objects = keys.map(v => ({ Key: v.trim() }));
		const { services } = await getCredential(req.body?.api_key);
		const s3 = await getS3Client(services.s3);
		const deleted_objects = await s3.client.deleteObjects({
			Bucket: s3.bucketName,
			Delete: { Objects: objects },
		}).promise();
		res.status(200).send({ deleted_objects });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

/**
 * Same bucket only!!!
 */
router.post('/copy-objects', uploads.single('file'), async (req, res, next) => {
	try {
		let { list } = req.body;
		if (!Array.isArray(list)) throw new Error('the list must be array');
		const { services } = await getCredential(req.body?.api_key);
		const s3 = await getS3Client(services.s3);
		let result = [];
		for (const item of list) {
			const theResult = await s3.client.copyObject({
				Bucket: s3.bucketName,
				CopySource: s3.bucketName + '/' + item.src,
				Key: item.dest,
			}).promise();
			result.push(theResult);
		}
		res.status(200).send({ result: result.map(v => v.CopyObjectResult) });
	} catch (err) {
		console.log('err', err);
		next(ApiError.badRequest(err));
	}
});

/**
 * Copy + Delete
 */
router.post('/move-objects', uploads.single('file'), async (req, res, next) => {
	try {
		let { list } = req.body;
		if (!Array.isArray(list)) throw new Error('the list must be array');
		const { services } = await getCredential(req.body?.api_key);
		const s3 = await getS3Client(services.s3);
		let copyResult = [];
		for (const item of list) {
			// Copy
			// ----------------------------------------
			const theResult = await s3.client.copyObject({
				Bucket: s3.bucketName,
				CopySource: s3.bucketName + '/' + item.src,
				Key: item.dest,
			}).promise();
			copyResult.push(theResult);
		}

		// Delete (Bulk)
		// ----------------------------------------
		const deleteResult = await s3.client.deleteObjects({
			Bucket: s3.bucketName,
			Delete: { Objects: list.map(v => ({ Key: v.src })) },
		}).promise();

		res.status(200).send({ copyResult, deleteResult });
	} catch (err) {
		console.log('err', err);
		next(ApiError.badRequest(err));
	}
});

// Upload .txt file with specifiable content. Mainly for debugging.
router.post('/upload-txt', async (req, res, next) => {
	try {
		const { key = '', text = '' } = req.body;
		await dbConnect();
		const { services } = await getCredential(req.body?.api_key);
		const s3 = await getS3Client(services.s3);
		const objects = await s3.client.upload({
			Bucket: s3.bucketName,
			Body: text,
			Key: key,
			// Prefix: prefix,
		}).promise();
		res.status(200).send({ objects });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;