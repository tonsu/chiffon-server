import express from 'express';
import ApiError from '../../error/ApiError.js';
import dbConnect from '../../configs/dbConnect.js';
import Credential from '../../models/Credential.js';

const router = express();
import dotenv from 'dotenv';

dotenv.config();

router.post('/', async (req, res, next) => {
	try {
		if(!(req.body && Object.keys(req.body).length)) throw new Error('body cannot be empty')
		await dbConnect();
		const credential = await Credential.create(req.body);
		res.status(200).send({ status: 'success', credential });
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});

export default router;
