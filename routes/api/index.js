import express from 'express';
import upload from './upload.js';
import videoEncode from "./video-encode.js";
import s3 from "./s3.js";
import databases from "./databases/index.js";

const router = express();

router.get('/', (req, res) => {
	res.send('Welcome Chiffon Server: v. image upload');
});

router.use('/upload', upload);
router.use('/video-encode', videoEncode);
router.use('/s3', s3);
router.use('/databases', databases);

export default router;