import express from 'express';
import ffmpeg from 'fluent-ffmpeg';
import dayjs from "dayjs";
import fs from "fs-extra";
import ApiError from "../../error/ApiError.js";
import { commonError } from "../../error/error-utils.js";

const router = express();

router.get('/', (req, res) => {
	res.send('Video upload!');
});

const defaultSettings = {
	outputPath: 'encoded',
	format: '.mp4',
};

router.post('/', async (req, res, next) => {
	try {
		const url = req.body.url;
		const settings = Object.assign({}, defaultSettings, req.body.settings);
		const { outputPath, format } = settings;
		const outputPathLocal = `./public/${outputPath}`;
		const filename = dayjs().format('YYMMDDHHmmss_SSS' + Math.floor(1000 + Math.random() * 9000));
		fs.mkdirsSync(outputPathLocal);
		await ffmpegSync(url, `${outputPathLocal}/${filename}${format}`, {});
		res.status(201).json({ filePath: `${outputPath}/${filename}${format}` });
	} catch (err) {
		next(ApiError.badRequest(commonError(err)));
	}
});

function ffmpegSync (url, outputPath, settings) {
	return new Promise((resolve, reject) => {
		ffmpeg(url)
			.output(outputPath)
			.videoCodec(settings?.codec || 'libx265')
			.on('end', function (stdout, stderr) {
				resolve();
			})
			.on('error', function (err, stdout, stderr) {
				return reject(new Error(err));
			})
			.run();
	});
}

export default router;