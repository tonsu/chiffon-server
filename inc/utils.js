
// add field if not exist
export function pushArrayField(obj, fieldName, content){
	if(!obj[fieldName]) obj[fieldName] = []
	obj[fieldName].push(content)
}