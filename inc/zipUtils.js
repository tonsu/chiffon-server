import fs from "fs";
import JSZip from 'node-zip';
import AdmZip from 'adm-zip';
import { mkdirNonExist } from "./fileUtils.js";

const TEMP_PATH = './public/temp';

export async function zipDir (zipPath, files) {
	const zip = new JSZip();

	for (const filePath of files) {
		const fileData = fs.readFileSync(filePath, 'utf8');
		const fileName = filePath.match(/[^\\/]+$/)[0];
		zip.file(fileName, fileData);
	}

	const data = zip.generate({ base64: false, compression: 'DEFLATE' });
	fs.writeFileSync(zipPath, data, 'binary');
	return zipPath;
}

// --------------------------------------------------------------------------------
// Unzip (Streaming)
// --------------------------------------------------------------------------------

export async function myUnzip (inputFile, callback) {
	mkdirNonExist(outPath);
	const zip = new AdmZip(inputFile);
	zip.extractAllTo(outPath, true);
}

export async function zipEntryWithCallback (inputFile, callback) {
	const zip = new AdmZip(inputFile);
	const zipEntries = zip.getEntries();

	zipEntries.forEach(callback);
}

export function getZipEntries (inputFile, excludes = []) {
	const zip = new AdmZip(inputFile);
	const zipEntries = zip.getEntries();
	const data = [];
	for (const zipEntry of zipEntries) {
		const name = zipEntry.entryName.split('.')[0];
		if (excludes.includes(name)) continue;
		data.push({
			name,
			content: zipEntry.getData().toString("utf8"),
		});
	}
	return data;
}