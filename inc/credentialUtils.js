import dbConnect from "../configs/dbConnect.js";
import Credential from "../models/Credential.js";

export async function getCredential (apiKey) {
	await dbConnect();
	return Credential.findOne({ key: apiKey });
}