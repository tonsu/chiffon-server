import User from "../models/User.js";
import S3 from "aws-sdk/clients/s3.js";
import dbConnect from "../configs/dbConnect.js";

// Legacy
// ----------------------------------------
export async function getBucketCredential (req) {
	await dbConnect();
	// todo - check for secret (access_id, access_secret)
	const user = await User.findOne({ user_id: req.query.id || req.query.access_id });
	if (!user) throw new Error('user not found');
	const bucket = user.credentials?.s3?.bucket;
	if (!bucket) throw new Error('bucket not found on the credential');
	const bucketClient = {
		client: new S3({
			region: bucket.region,
			accessKeyId: bucket.id,
			secretAccessKey: bucket.secret,
		}),
		bucketName: bucket.bucket_name,
	};
	return bucketClient;
}