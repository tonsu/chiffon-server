import S3 from 'aws-sdk/clients/s3.js';
import dotenv from 'dotenv';

dotenv.config();
import fs from 'fs';
import dbConnect from "../configs/dbConnect.js";

// The Client
// ----------------------------------------
export async function getS3Client (s3) {
	await dbConnect();
	return {
		client: new S3({
			region: s3.region,
			accessKeyId: s3.id,
			secretAccessKey: s3.secret,
		}),
		bucketName: s3.bucket_name,
	};
}

// Upload
// ----------------------------------------
export function uploadFileToBucket (clientData, setting) {
	if(!setting?.resultPath) throw new Error('no result path for bucket')
	let fileBinaryString = fs.readFileSync(setting.resultPath);

	const uploadParams = {
		Bucket: clientData.bucketName,
		Body: fileBinaryString,
		Key: setting.bucketPath,
	};
	return clientData.client.upload(uploadParams).promise();
}