import { MongoClient } from "mongodb";

export class MongoMine {
	constructor (uri, dbName) {
		this.dbName = dbName;
		this.uri = uri;
	}

	connect () {
		this.client = new MongoClient(this.uri);
	}

	async listDatabases () {
		const databasesList = await this.client.db().admin().listDatabases();
		return databasesList;
	}

	async listCollections () {
		const cursor = await this.client.db(this.dbName).listCollections({}, { nameOnly: true });
		return await cursor.toArray();
	}

	async find (params) {
		// console.log('- count', await this.client.db(this.dbName)
		// 	.collection(params.collection).countDocuments(params?.query || {}));

		let cursor = await this.client.db(this.dbName).collection(params.collection)
			.find(params?.query || {})
			.sort(params?.sort || null)
			.limit(params?.limit || 0);

		return cursor.toArray();
	}

	async deleteMany (params) {
		let result = await this.client.db(this.dbName).collection(params.collection)
			.deleteMany(params?.query || {});
		return result;
	}

	async deleteOne (params) {
		let result = await this.client.db(this.dbName).collection(params.collection)
			.deleteOne(params?.query || {});
		return result;
	}

	async create (docs, params) {
		if (!docs.length) return { message: 'empty' };
		let result = await this.client.db(this.dbName).collection(params.collection)
			.insertMany(docs);
		return result;
	}

	async reindex (params) {
		let result = await this.client.db(this.dbName).collection(params.collection)
		return result;
	}

	async close () {
		await this.client?.close();
	}

}