import mongoose from "mongoose";

const schema = new mongoose.Schema({
	struct_id: {
		type: String,
		required: [true, 'Required'],
	},
	data: {
		type: Object,
	},
	fields: {
		type: Array,
	},
}, { timestamps: true });

export default (mongoose.models && mongoose.models.Structure) || mongoose.model('Structure', schema);