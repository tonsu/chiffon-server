import mongoose from 'mongoose';

const schema = new mongoose.Schema({
	key: {
		type: String,
	},
	slug: {
		type: String,
	},
	services: {
		type: Object,
	}
}, { timestamps: true });

schema.index({ "key": 1 });

export default mongoose.model('Credential', schema);
