import mongoose from 'mongoose'

/* PetSchema will correspond to a collection in your MongoDB database. */
const Schema = new mongoose.Schema({
  key: {
		type: String,
	  required: [true, 'Must provide the key.'],
  },
	value: {
		type: Object,
	}
})

export default mongoose.models.Global || mongoose.model('Global', Schema)
