import bcrypt from 'bcryptjs';
import mongoose from 'mongoose';
import mongoosePaginate from "mongoose-paginate";

/* Alternative to the 'match' field */
const validateEmail = (email) => {
	const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	return re.test(email);
};

/* PetSchema will correspond to a collection in your MongoDB database. */
const schema = new mongoose.Schema({
	user_id: {
		type: String,
		required: 'UserID is required',
	},
	username: {
		type: String,
		required: 'Username is required',
	},
	email: {
		type: String,
		trim: true,
		lowercase: true,
		// required: 'Email address is required',
		// validate: [validateEmail, 'Please fill a valid email address'],
		match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
	},
	display_name: {
		type: String,
	},
	password: {
		type: String,
		minlength: [4, "Password should be at least 6 characters"],
	},
	roles: {
		type: Array,
	},
	credentials: {
		type: Object,
	},
}, { timestamps: true });

schema.pre('save', function (next) {
	let user = this;

	// only hash the password if it has been modified (or is new)
	if (!user.isModified('password')) return next();

	bcrypt.hash(user.password, parseInt(process.env.SALT_WORK_FACTOR), function (err, hash) {
		if (err) return next(err);
		user.password = hash;
		next();
	});
});

schema.methods.comparePassword = function (candidatePassword, cb) {
	bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
		if (err) return cb(err);
		cb(null, isMatch);
	});
};

schema.index({ "email": 1 });
schema.index({ "username": 1 }, { unique: true });
schema.index({ "user_id": 1 }, { unique: true });

schema.plugin(mongoosePaginate);
export default mongoose.model('User', schema);
