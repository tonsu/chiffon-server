export function commonError (error) {
	if (error.name === 'MongoError' && error.code === 11000) {
		return 'The email or username is already taken.';
	}
	return error;
}

export function getErrorMessage (err) {
	if (typeof err === 'string') return err;
	if (err?.message) return err?.message;
	return err;
}
