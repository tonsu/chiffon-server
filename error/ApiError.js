import { getErrorMessage } from "./error-utils.js";

class ApiError {
	constructor (code, message) {
		this.code = code;
		this.message = message;
		this.fromApiError = true;
	}

	static badRequest (msg) {
		return new ApiError(400, getErrorMessage(msg));
	}

	static internal (msg) {
		return new ApiError(500, getErrorMessage(msg));
	}
}

export default ApiError
