import ApiError from './ApiError.js';

export default function apiErrorHandler (err, req, res, next) {
	// in prod, don't use console.log or console.err because it is not async
	console.log(err);

	// Note: this will always be false, weird, better check the param instead
	// if (err instanceof ApiError) {
	// 	res.status(err.code).json(err.message);
	// 	return;
	// }

	if (err.fromApiError) {
		res.status(err.code).json({
			message: err.message,
		});
		return;
	}
	res.status(500).json('something went wrong');
}

