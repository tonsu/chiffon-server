import fs from 'fs-extra';
import sharp from 'sharp';
import axios from "axios";
import { uploadFileToBucket } from "./s3_RDG.js";

const TEMP_PATH = 'public/temp';
export default async function (params) {
	let ret = [];

	let buffer;

	if (params.src) {
		const imageResponse = await axios({ url: params.src, responseType: 'arraybuffer' });
		buffer = Buffer.from(imageResponse.data, 'binary');
	} else {
		buffer = params.file.buffer;
	}

	// add suffix if duplicated name
	// ----------------------------------------

	for (const param of params.settings) {

		let fullPath = (param.path + '/' + param.name).replace('//', '/');
		let fullPathTemp = (TEMP_PATH + '/' + param.name).replace('//', '/');
		fs.mkdirsSync(TEMP_PATH);

		let pipe = sharp(buffer, { failOnError: false });
		if (param.colorSpace) {
			pipe = pipe.toColourspace(param.colorSpace);
		}
		if (param.resize) {
			pipe = pipe.resize(param.resize);
		}
		if (param.jpeg) {
			pipe = pipe.toFormat('jpeg');
			if (param.metadata) {
				pipe = pipe.withMetadata(param.metadata);
			}
			pipe = pipe.jpeg(param.jpeg);
		}
		const file = await pipe.toFile(fullPathTemp);

		const resUpload = await uploadFileToBucket(fullPathTemp, fullPath, param.bucketName);
		ret.push({
			fullPath,
		});
		fs.unlinkSync(fullPathTemp);
	}

	return ret;
};

function spiltPathAndFileName (url) {
	let segments = url.split('/');
	let fileName = segments.splice(-1);
	return [segments.join('/'), fileName];
}
