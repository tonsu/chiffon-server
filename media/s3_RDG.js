import S3 from 'aws-sdk/clients/s3.js';
import dotenv from 'dotenv';

dotenv.config();
import fs from 'fs';

const BUCKETNAME = process.env.AWS_BUCKET_NAME;
const region = process.env.AWS_BUCKET_REGION_2;
const accessKeyId = process.env.AWS_ID_2;
const secretAccessKey = process.env.AWS_SECRET_2;

const s3 = new S3({
	region,
	accessKeyId,
	secretAccessKey,
});

export function uploadFileToBucket (file, uploadPath, bucketName) {
	let fileBinaryString = fs.readFileSync(file);

	const uploadParams = {
		Bucket: bucketName || BUCKETNAME,
		Body: fileBinaryString,
		Key: uploadPath,
	};

	return s3.upload(uploadParams).promise();

}

export function getFileStream (fileKey, bucketName=BUCKETNAME) {
	const downloadParams = {
		Key: fileKey,
		Bucket: BUCKETNAME,
	};
	return s3.getObject(downloadParams).createReadStream();
}

