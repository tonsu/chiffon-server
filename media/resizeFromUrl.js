import fs from 'fs-extra'
import sharp from 'sharp'
import axios from "axios";

export default async function (src) {
	let out = [];
	const imageResponse = await axios({ url: src, responseType: 'arraybuffer' });

	const buffer = Buffer.from(imageResponse.data, 'binary');
	const today = new Date();
	const year = today.getFullYear();
	const month = `${today.getMonth() + 1}`.padStart(2, "0");
	const uploadPath = `./public/uploads/${year}/${month}`;
	// const testFileStr = 'http://arst.com/upload/test-image_และ-123[1](2).webp';
	let [fileName, _fileExtension] = getFilenameAndExtension(src);
	let fileExtension = _fileExtension;
	fs.mkdirsSync(uploadPath);

	// add suffix if duplicated name
	// ----------------------------------------

	let suffix = '';
	if (fs.existsSync(`${uploadPath}/${fileName}${suffix}.${fileExtension}`)) {
		for (let idx = 1; idx < 99; idx++) {
			suffix = '-' + idx;
			if (!fs.existsSync(`${uploadPath}/${fileName}${suffix}.${fileExtension}`)) break;
		}
	}

	// Resize
	// ----------------------------------------

	let sizes = [
		{
			fileName: `${uploadPath}/${fileName}${suffix}.${fileExtension}`,
		},
		{
			fileName: `${uploadPath}/${fileName}${suffix}-sm.${fileExtension}`,
			resize: {
				width: 150,
				height: 150,
			},
		},
		{
			fileName: `${uploadPath}/${fileName}${suffix}-lg.${fileExtension}`,
			resize: {
				width: 1080,
				height: 1080,
				fit: sharp.fit.inside,
				withoutEnlargement: true,
			},
		},
	];

	out.push(`${uploadPath}/${fileName}${suffix}.${fileExtension}`); // output only original

	for (const size of sizes) {
		if (size.resize) {
			await sharp(buffer).resize(size.resize).toFile(size.fileName);
		} else {
			await sharp(buffer).toFile(size.fileName);
		}
		// out.push(size.fileName) // output all sizes
	}

	await sharp(buffer).toFile(`${uploadPath}/${fileName}${suffix}.${fileExtension}`);
	await sharp(buffer).resize({ width: 150, height: 150 }).toFile(`${uploadPath}/${fileName}${suffix}-xs.${fileExtension}`);
	await sharp(buffer).resize({
		width: 1080,
		height: 1080,
		fit: sharp.fit.inside,
		withoutEnlargement: true,
	}).toFile(`${uploadPath}/${fileName}${suffix}-lg.${fileExtension}`);

	return out;
};

function getFilenameAndExtension (str) {
	try {
		let segments = str.split('/');
		let fileSegment = segments[segments.length - 1].split('.');
		let ext = fileSegment.pop();
		let name = fileSegment.join('_');
		return [name, ext];
	} catch (err) {
		console.log('[getFilenameAndExtension]', err);
		throw err;
	}
}
