import fs from 'fs-extra';
import sharp from 'sharp';
import axios from "axios";

const TEMP_PATH = 'public/temp';

const getFilePath = (fullPath) => {
  const segments = fullPath.split('/')
	segments.pop()
	return segments.join('/')
}

export default async function (input, setting) {
	let buffer;

	if (input.src) {
		const imageResponse = await axios({ url: input.src, responseType: 'arraybuffer' });
		buffer = Buffer.from(imageResponse.data, 'binary');
	} else {
		buffer = input.file.buffer;
	}

	const fullPath = setting.path

	fs.mkdirsSync(getFilePath(fullPath));

	let pipe = sharp(buffer, { failOnError: false });
	if (setting.colorSpace) {
		pipe = pipe.toColourspace(setting.colorSpace);
	}
	if (setting.resize) {
		pipe = pipe.resize(setting.resize);
	}
	if (setting.jpeg) {
		pipe = pipe.toFormat('jpeg');
		if (setting.metadata) {
			pipe = pipe.withMetadata(setting.metadata);
		}
		pipe = pipe.jpeg(setting.jpeg);
	}
	const file = await pipe.toFile(fullPath);
	setting.resultPath = fullPath;

	return fullPath;
};