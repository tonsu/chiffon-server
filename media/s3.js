import S3 from 'aws-sdk/clients/s3.js';
import dotenv from 'dotenv';

dotenv.config();
import fs from 'fs';

// --------------------------------------------------------------------------------
// Legacy
// --------------------------------------------------------------------------------

const BUCKETNAME = process.env.AWS_BUCKET_NAME;
const region = process.env.AWS_BUCKET_REGION;
const accessKeyId = process.env.AWS_ID;
const secretAccessKey = process.env.AWS_SECRET;

const s3 = new S3({
	region,
	accessKeyId,
	secretAccessKey,
});

export function uploadFileToBucket (file, uploadPath, bucketName) {
	let fileBinaryString = fs.readFileSync(file);

	const uploadParams = {
		Bucket: bucketName || BUCKETNAME,
		Body: fileBinaryString,
		Key: uploadPath,
	};

	return s3.upload(uploadParams).promise();

}

export function getFileStream (fileKey, bucketName = BUCKETNAME) {
	const downloadParams = {
		Key: fileKey,
		Bucket: BUCKETNAME,
	};
	return s3.getObject(downloadParams).createReadStream();
}

// --------------------------------------------------------------------------------
// New (user's)
// --------------------------------------------------------------------------------

export function uploadFileToUserBucket ({
	                                        file,
	                                        fileBinary,
	                                        uploadPath,
	                                        bucketName,
	                                        region,
	                                        accessKeyId,
	                                        secretAccessKey,
                                        }) {
	let fileBinaryString = fileBinary || fs.readFileSync(file);
	const s3 = new S3({
		region,
		accessKeyId,
		secretAccessKey,
	});
	const uploadParams = {
		Bucket: bucketName,
		Body: fileBinaryString,
		Key: uploadPath,
	};
	return s3.upload(uploadParams).promise();
}