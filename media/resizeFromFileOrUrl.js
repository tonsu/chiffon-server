import fs from 'fs-extra'
import sharp from 'sharp'
import axios from "axios";

export default async function (params) {
	let ret = {};
	// let ret = [];

	let buffer;
	if (params.src) {
		const imageResponse = await axios({ url: params.src, responseType: 'arraybuffer' });
		buffer = Buffer.from(imageResponse.data, 'binary');
	} else {
		buffer = params.file.buffer;
	}

	// add suffix if duplicated name
	// ----------------------------------------

	for (const output of params.outputs) {
		let fullPath = output.fullPath;
		fs.mkdirsSync(spiltPathAndFileName(fullPath)[0]);

		if (output.overwrite) {
			fullPath = output.fullPath.replace('$EXT$', output.ext).replace('$DUPLICATE$', '');
		} else {
			if (fullPath.includes('$DUPLICATE$')) {
				for (let idx = 0; idx < 99; idx++) {
					if (output.ext) fullPath = output.fullPath.replace('$EXT$', output.ext);
					fullPath = output.fullPath.replace('$DUPLICATE$', idx > 0 ? '-' + idx : '');
					if (!fs.existsSync(fullPath)) break;
				}
			}
		}

		const split = fullPath.split('/');

		ret[output.name] = {
			path: fullPath.replace('./public', ''),
			serverPath: fullPath,
			name: split[split.length - 1],
		}

		// ret.push({
		// 	path: fullPath.replace('./public', ''),
		// 	serverPath: fullPath,
		// 	name: split[split.length - 1]
		// });

		let file = null;
		if (output.resize) {
			file = await sharp(buffer).resize(output.resize).toFile(fullPath);
		} else {
			file = await sharp(buffer).toFile(fullPath);
		}
	}

	return ret;
};

function spiltPathAndFileName (url) {
	let segments = url.split('/');
	let fileName = segments.splice(-1);
	return [segments.join('/'), fileName];
}
