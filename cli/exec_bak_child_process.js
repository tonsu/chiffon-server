const { exec } = require('child_process');

/* For exec the.sh file */

exec('pwd', (err, stdout, stderr) => {
	if (err) {
		console.log('err.message', err.message);
	}
	if (stderr) {
		console.log('stderr', stderr);
	}
	console.log('stdout', stdout);
});

exec('ls -lh', (err, stdout, stderr) => {
	if (err) {
		console.log('err.message', err.message);
	}
	if (stderr) {
		console.log('stderr', stderr);
	}
	console.log('stdout', stdout);
});
