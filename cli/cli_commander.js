const weGetData = require('../migrate/wpGetData');
const program = require('commander');

program.version('0.0.1');

// program
// 	.command('say')
// 	.description('This cmd will say something.')
// 	.alias('s')
// 	.action(function (name){
// 		console.log('name', name);
// 	})
//
//
// program
// 	.option('-d, --do', 'Do something')
// program.parse(process.argv)

// function main () {
// 	process.argv.forEach(function (val, index, array) {
// 		console.log(index + ': ' + val);
// 	});
//
// }

// Action handler
// ----------------------------------------

// program
// 	.argument('<name>')
// 	.option('-t, --title <honorific>', 'title to use before name')
// 	.option('-d, --debug', 'display some debugging')
// 	.action((name, options, command) => {
// 		if (options.debug) {
// 			console.error('Called %s with options %o', command.name(), options);
// 		}
// 		const title = options.title ? `${options.title} ` : '';
// 		console.log(`Thank-you ${title}${name}`);
// 	});

// Simple Args
// ----------------------------------------

// program
// 	.option('-d, --debug', 'output extra debugging')
// 	.option('-s, --small', 'small pizza size')
// 	.option('-p, --pizza-type <type>', 'flavour of pizza');
//
// program.parse(process.argv);
//
// const options = program.opts();
// if (options.debug) console.log(options);
// console.log('pizza details:');
// if (options.small) console.log('- small pizza size');
// if (options.pizzaType) console.log(`- ${options.pizzaType}`);

// My Take
// ----------------------------------------

program
	.option('-l, --limit <limit>', 'model limit');

program.parse(process.argv);

async function getModels () {
	const options = program.opts();
	const limit = options.limit || 999;
	const res = await weGetData(limit);

	// console.log('res', res);
	console.log('res', res.taxes);

}

getModels();


