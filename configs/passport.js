const passport = require('passport');
const { pool } = require('../configs/connection');
const { comparePassword } = require('../db/utils');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
require('dotenv').config();

passport.use(new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
	},
	(username, password, cb) => {
		//this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT

		pool.connect(async (err, client, done) => {
			try {
				let result = await client.query(`SELECT id, login, email, display_name, pass
                                         FROM kv_users
                                         WHERE login = $1`, [username]);
				if (!result.rows.length) {
					cb(null, false, { message: 'Incorrect email or password.' });
					return;
				}

				if (comparePassword(password, result.rows[0].pass)) {

					// Note: for NextAuth, makes field the same as other auth method (e.g. Google)
					cb(null, {
						name: result.rows[0].display_name,
						email: result.rows[0].email,
					}, { message: 'Logged In Successfully' });
				} else {
					cb(null, false, { message: 'Incorrect email or password.' });
				}
				// res.status(200).send({
				// 	data: result.rows || result,
				// });
			} catch (err) {
				cb(null, false, { message: 'Incorrect email or password.' });
			} finally {
				done();
			}
		});

		// return UserModel.findOne({ email, password })
		// 	.then(user => {
		// 		if (!user) {
		// 			return cb(null, false, { message: 'Incorrect email or password.' });
		// 		}
		// 		return cb(null, user, { message: 'Logged In Successfully' });
		// 	})
		// 	.catch(err => cb(err));
	},
));

passport.use(new JWTStrategy({
		jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
		secretOrKey: process.env.JWT_SECRET,
	},
	(jwtPayload, cb) => {
		//find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.

		// todo: not sure what to do here
		// console.log('jwtPayload', jwtPayload);
		cb(null, {
			login: jwtPayload.name,
			debug: 'This is from my configs/passport.js',
		});
		// return UserModel.findOneById(jwtPayload.id)
		// 	.then(user => {
		// 		return cb(null, user);
		// 	})
		// 	.catch(err => {
		// 		return cb(err);
		// 	});
	},
));
