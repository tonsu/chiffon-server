import * as pg from 'pg'
const { Pool } = pg

/**
 * Unsolved problem from upgrading to es6
 */
// export const pool = new Pool({
// 	user: 'devartplore', //this is the db user credential
// 	database: 'nancy',
// 	password: 'dev@artplore@local',
// 	port: 5432,
// 	max: 10, // max number of pools in the pool
// 	idleTimeoutMillis: 30000,
// });

export const pool = null
