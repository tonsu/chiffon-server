import { pool } from '../configs/connection.js';
import casual from 'casual';
import bcrypt from "bcryptjs";

function logger (val) {
	console.log('[seedUser]', val);
}

export default async function () {

	let log = [];
	let limitUser = 20;
	casual.seed(123);

	const hashedPassword = await new Promise((resolve, reject) => {
		bcrypt.hash('arstarst', process.env.SALT_WORK_FACTOR, function (err, hash) {
			if (err) reject(err);
			resolve(hash);
		});
	});

	await pool.connect(async (err, client, done) => {
		try {
			await client.query('BEGIN');
			for (let i = 1; i <= limitUser; i++) {
				let users = [
					{
						login: `dummy-${i}`,
						pass: hashedPassword,
						nice_name: `dummy-${i}`,
						email: `dummy.${i}.manydev@gmail.com`,
						url: '',
						activation_key: '',
						display_name: 'Nancy Admin',
						status: 7,
					},
				];
				for (const user of users) {
					let result = await client.query(`INSERT INTO kv_users(login, pass, nice_name, email, url, activation_key, display_name, status)
                                           VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                                           RETURNING id`, [user.login, user.pass, user.nice_name, user.email, user.url, user.activation_key, user.display_name, user.status]);

					log.push(result.rows[0].id);
				}
			}
			await client.query('COMMIT');
		} catch (err) {
			await client.query('ROLLBACK');
			throw err;
		} finally {
			logger('finish');
			done();
		}
	});
	return log;
};

