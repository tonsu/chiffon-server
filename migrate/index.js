import express from "express";
import ApiError from '../error/ApiError.js'
const router = express();

router.get('/create-tables', async (req, res, next) => {
	try {
		let log = await require('../db/createTables')()
		res.status(200).send({
			log,
		});
	} catch (err) {
		next(ApiError.badRequest(err));
	}
});
router.get('/seed', async (req, res, next) => {
	try {
		let log = await require('./seed')()
		res.status(200).send({
			log,
		});
	} catch (err) {
		console.log('err', err);
		next(ApiError.badRequest(err));
	}
});

router.get('/seed-users', async (req, res, next) => {
	try {
		let log = await require('./seedUsers')()
		res.status(200).send({
			log,
		});
	} catch (err) {
		console.log('err', err);
		next(ApiError.badRequest(err));
	}
});

router.get('/dummy-vote', async (req, res, next) => {
	try {
		let log = await require('./dummyVote')()
		res.status(200).send({
			log,
		});
	} catch (err) {
		console.log('err', err);
		next(ApiError.badRequest(err));
	}
});
router.get('/get-missing-images', async (req, res, next) => {
	try {
		const log = await require('./getMissingImages')()
		res.status(200).send({
			log,
		});
	} catch (err) {
		console.log('err', err);
		next(ApiError.badRequest(err));
	}
});
export default router;
