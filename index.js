import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import router from './routes/index.js'
import migrate from './migrate/index.js'
import apiErrorHandler from './error/api-error-handler.js'

const app = express();

// app.use(cors({
// 	origin: [
// 		'*',
// 	],
// 	credentials: true,
// }));

app.use(cors({
	origin: [
		'http://192.168.2.44:3201',
		/\/\/192\.168\.\d+\.\d+:3\d\d\d/,
		'https://nancy.gookgoo.com',
		'https://messi-three.vercel.app', // why #A is not enough?
		/localhost:32\d\d$/,
		/\.artplore\.com$/,
		/\.gookgoo\.com$/,
		/\.mickeydeal\.com$/,
		/\.vercel\.app$/, // #A
		/\.auramagicz\.com$/,
		/\.averyth\.com$/,
		/\.rabbitdigitalgroup\.com$/,
		/register\.*\.com$/,
	],
	credentials: true,
}));

// for line.middleware, must be before bodyparser (for SDK only)
// app.use('/linebots', lineBots);

app.use(express.static('public'));
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit:'50mb' }));
app.use('/', router);
app.use('/migrate', migrate);

// let arst = 'arst.arst.arst'
// console.log('---', arst.replace(/(\.[\w]{3,4}$)/i, '-xs$1'));

app.use(apiErrorHandler);

const port = process.env.PORT || 3303;

app.listen(port, () => {
	console.log(`We are live - at 127.0.0.1:${port}`);
});

// const filePath = './public/uploads/models/210726205907_4465109-sm.png'
// let fileBinaryString = fs.readFileSync(filePath)
// console.log('fileBinaryString', fileBinaryString);
